|         Variable name          |      Default value      |                      Value type                      | Description |
| :----------------------------: | :---------------------: | :--------------------------------------------------: | ----------- |
|       `NEXT_PUBLIC_HOST`       | `http://localhost:3000` |                       `String`                       | -           |
| `NEXT_PUBLIC_FRONTEND_VERSION` |           `-`           |                       `String`                       | -           |
|       `BACKEND_VERSION`        |           `-`           |                       `String`                       | -           |
|         `DATABASE_URL`         |           `-`           | `mysql://user:password@localhost:3306/database_name` | -           |
